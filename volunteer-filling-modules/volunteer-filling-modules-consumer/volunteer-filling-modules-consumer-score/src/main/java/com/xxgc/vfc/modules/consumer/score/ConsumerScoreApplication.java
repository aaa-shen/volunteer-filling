package com.xxgc.vfc.modules.consumer.score;

import com.xxgc.vfc.common.swagger.config.EnableCustomSwagger2;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@EnableCustomSwagger2
@EnableDubbo
//激活通用AOP
@SpringBootApplication
//指定开始扫描的路径
@ComponentScan(basePackages={"com.xxgc.vfc"})
public class ConsumerScoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerScoreApplication.class, args);
    }

}
