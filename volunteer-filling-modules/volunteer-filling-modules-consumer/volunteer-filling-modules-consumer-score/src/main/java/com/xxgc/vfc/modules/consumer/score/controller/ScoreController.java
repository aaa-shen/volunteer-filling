package com.xxgc.vfc.modules.consumer.score.controller;

import com.xxgc.vfc.api.result.Result;
import com.xxgc.vfc.api.service.IScoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Sjy
 * @Date :2023/10/20 - 10 - 20 - 14:41
 */
@Api(tags = "分数接口")
@RestController
@RequestMapping("/score")
public class ScoreController {

    @DubboReference(interfaceClass = IScoreService.class,version = "0.0.1")
    private IScoreService iScoreService;

    @ApiOperation(value = "查询分数")
    @GetMapping("/getMessage")
    public Result getMessage(){
        Integer scoreByUserIdCard = iScoreService.getScoreByUserIdCard("1");
        return Result.ok("分数："+scoreByUserIdCard).msg("Hello Spring Cloud Alibaba");
    }

}
