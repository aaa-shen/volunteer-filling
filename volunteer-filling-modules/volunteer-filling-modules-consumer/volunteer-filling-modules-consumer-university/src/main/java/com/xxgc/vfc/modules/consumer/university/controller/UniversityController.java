package com.xxgc.vfc.modules.consumer.university.controller;

import com.xxgc.vfc.api.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: Sjy
 * @Date :2023/10/20 - 10 - 20 - 16:39
 * 院校接口
 */
@Api(tags = "学校接口")
@RestController
@RequestMapping("/university")
public class UniversityController {
    public static Integer id = 0;

    @ApiOperation(value = "获取学校名称")
    @GetMapping("/getUniversityName")
    public Result getUniversityName(){
        id ++;
        System.out.println("请求"+id);
        return Result.ok().msg("成都信息工程大学");
    }

    @ApiOperation(value = "添加学校")
    @PostMapping("/addUniversity")
    public Result addUniversity(){
        return Result.ok().msg("1111");
    }
}
