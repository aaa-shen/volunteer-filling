package com.xxgc.vfc.modules.provider.bd.service;

import com.xxgc.vfc.api.service.IScoreService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @Author: Sjy
 * @Date :2023/10/20 - 10 - 20 - 17:29
 */
@DubboService(interfaceClass = IScoreService.class,version = "0.0.1")
public class ScoreServiceImpl implements IScoreService {
    @Override
    public Integer getScoreByUserIdCard(String idCard) {
        return 100;
    }
}
