package com.xxgc.vfc.modules.provider.bd.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 学校表
 * </p>
 *
 * @author 风清扬
 * @since 2023-11-03
 */
@Getter
@Setter
@TableName("vfc_schools")
@ApiModel(value = "Schools对象", description = "学校表")
public class Schools implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("学校名称")
    private String schoolName;

    @ApiModelProperty("年份")
    private LocalDate schoolYear;

    @ApiModelProperty("0大专 1是一本 2 是二本 ")
    private Integer level;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty("有效字段")
    @TableLogic
    private Boolean flag;
}
