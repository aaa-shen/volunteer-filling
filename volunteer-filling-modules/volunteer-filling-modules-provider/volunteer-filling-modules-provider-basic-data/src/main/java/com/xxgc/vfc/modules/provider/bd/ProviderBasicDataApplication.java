package com.xxgc.vfc.modules.provider.bd;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@EnableDubbo
@SpringBootApplication
//指定开始扫描的路径
@ComponentScan(basePackages={"com.xxgc.vfc"})
public class ProviderBasicDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProviderBasicDataApplication.class, args);
    }

}
