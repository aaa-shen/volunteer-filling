package com.xxgc.vfc.modules.provider.bd.mapper;

import com.xxgc.vfc.modules.provider.bd.entity.Schools;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 学校表 Mapper 接口
 * </p>
 *
 * @author 风清扬
 * @since 2023-11-03
 */
public interface SchoolsMapper extends BaseMapper<Schools> {

}
