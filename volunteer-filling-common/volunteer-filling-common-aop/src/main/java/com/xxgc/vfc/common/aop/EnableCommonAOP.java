package com.xxgc.vfc.common.aop;

import com.xxgc.vfc.common.aop.common.SystemInfoResultAOP;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/*
 *
 *   @Author:SJY
 *   @Date: 2023/10/25-10-25-11:40
 *
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({//有哪些类需要被IOC容器装填
        SystemInfoResultAOP.class
})
public @interface EnableCommonAOP {
}
