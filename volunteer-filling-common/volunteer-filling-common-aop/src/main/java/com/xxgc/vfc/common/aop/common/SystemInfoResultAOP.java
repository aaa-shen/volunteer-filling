package com.xxgc.vfc.common.aop.common;

import com.xxgc.vfc.api.result.Result;
import com.xxgc.vfc.common.aop.po.SystemInfoPO;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
 *
 *   @Author: Sjy
 *   @Date: 2023/10/25-10-25-11:21
 *   给所有接口返回的时候加一个版本号
 */
@Component
@Aspect
public class SystemInfoResultAOP {

    @Autowired
    private SystemInfoPO systemInfoPO;

    @Around("execution(* com.xxgc.vfc.modules.consumer.*.controller.*Controller.*(..))")
    public Object addSystemInfoToResult(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("我成功拦截了");
        /**
         * 作业：
         * 拦截请求后把配置中心的版本号和项目名称
         * 填写到响应结果Result里面再返回请求
         */
        Result result = (Result) proceedingJoinPoint.proceed();

        result.systemName(systemInfoPO.getSystemName()).systemVersion(systemInfoPO.getSystemVersion());

        return result;
    }

}
