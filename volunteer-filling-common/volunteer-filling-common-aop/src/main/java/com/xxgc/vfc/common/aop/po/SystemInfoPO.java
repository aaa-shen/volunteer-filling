package com.xxgc.vfc.common.aop.po;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @Author: Sjy
 * @Date :2023/10/26 - 10 - 26 - 21:07
 */
@Data
@Component
//实现配置中心的配置动态更新
@RefreshScope
public class SystemInfoPO {
    @Value("${vfc.system.name}")
    private String systemName;
    @Value("${vfc.system.version}")
    private String systemVersion;
}
