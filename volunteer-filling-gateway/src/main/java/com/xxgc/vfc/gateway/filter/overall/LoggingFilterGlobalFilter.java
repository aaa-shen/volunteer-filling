package com.xxgc.vfc.gateway.filter.overall;
/*
 *
 *   @Author: Sjy
 *   @Date: 2023/11/1-11-01-11:43
 *   全局日志过滤器
 *   名字规范
 *   自己过滤器的名字 + Global + Filter
 */

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import reactor.core.publisher.ParallelFlux;

@Slf4j
@Component
public class LoggingFilterGlobalFilter implements GlobalFilter {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        RequestPath path = request.getPath();
        //请求路径
        String s = path.toString();
        //响应状态码
        HttpStatus statusCode = response.getStatusCode();
        //获取带参


        //获取响应结果

        //获取请求头信息

        log.info("过滤器请求路径："+s);
        log.info("状态码:"+statusCode.value());
        log.info("请求参数:");

        //存入数据库0

       //请求放行
        return chain.filter(exchange);
    }
}
