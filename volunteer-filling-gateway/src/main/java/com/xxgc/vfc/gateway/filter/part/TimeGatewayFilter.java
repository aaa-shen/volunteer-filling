package com.xxgc.vfc.gateway.filter.part;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
public class TimeGatewayFilter implements GatewayFilter {

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //获取当前时间
        long startTime = System.currentTimeMillis();
        //放行
        return chain.filter(exchange).then(Mono.fromRunnable(()->{
            //回调后的内容
            long stopTime = System.currentTimeMillis();
            long time = stopTime - startTime;
            log.info(String.valueOf(time));
        }));
    }
}
