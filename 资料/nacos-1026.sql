/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50743
Source Host           : localhost:3306
Source Database       : nacos

Target Server Type    : MYSQL
Target Server Version : 50743
File Encoding         : 65001

Date: 2023-10-26 21:43:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for config_info
-- ----------------------------
DROP TABLE IF EXISTS `config_info`;
CREATE TABLE `config_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `content` longtext COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text COLLATE utf8_bin COMMENT 'source user',
  `src_ip` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `tenant_id` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `c_use` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `effect` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `type` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `c_schema` text COLLATE utf8_bin,
  `encrypted_data_key` text COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_configinfo_datagrouptenant` (`data_id`,`group_id`,`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info';

-- ----------------------------
-- Records of config_info
-- ----------------------------
INSERT INTO `config_info` VALUES ('2', 'consumer-score-dev.yaml', 'DEFAULT_GROUP', 0x61613A, '17f9a2997fe1e5996eb84b0e1cfe6d89', '2023-10-25 02:01:37', '2023-10-26 12:40:06', 0x6E61636F73, '192.168.0.151', '', '', '分数服务的配置文件', '', '', 'yaml', '', '');
INSERT INTO `config_info` VALUES ('6', 'system-info-dev.yaml', 'DEFAULT_GROUP', 0x7666633A0A2020202073797374656D3A0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F320A202020202020202076657273696F6E3A2076302E302E37, '8cd3ebc0c0134de750a54282333e3799', '2023-10-26 12:41:37', '2023-10-26 13:20:06', 0x6E61636F73, '192.168.0.151', '', '', '项目信息配置文件（全局）', '', '', 'yaml', '', '');

-- ----------------------------
-- Table structure for config_info_aggr
-- ----------------------------
DROP TABLE IF EXISTS `config_info_aggr`;
CREATE TABLE `config_info_aggr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `datum_id` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'datum_id',
  `content` longtext COLLATE utf8_bin NOT NULL COMMENT '内容',
  `gmt_modified` datetime NOT NULL COMMENT '修改时间',
  `app_name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `tenant_id` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_configinfoaggr_datagrouptenantdatum` (`data_id`,`group_id`,`tenant_id`,`datum_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='增加租户字段';

-- ----------------------------
-- Records of config_info_aggr
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_beta
-- ----------------------------
DROP TABLE IF EXISTS `config_info_beta`;
CREATE TABLE `config_info_beta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'app_name',
  `content` longtext COLLATE utf8_bin NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) COLLATE utf8_bin DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text COLLATE utf8_bin COMMENT 'source user',
  `src_ip` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` text COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_configinfobeta_datagrouptenant` (`data_id`,`group_id`,`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info_beta';

-- ----------------------------
-- Records of config_info_beta
-- ----------------------------

-- ----------------------------
-- Table structure for config_info_tag
-- ----------------------------
DROP TABLE IF EXISTS `config_info_tag`;
CREATE TABLE `config_info_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'app_name',
  `content` longtext COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `src_user` text COLLATE utf8_bin COMMENT 'source user',
  `src_ip` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_configinfotag_datagrouptenanttag` (`data_id`,`group_id`,`tenant_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info_tag';

-- ----------------------------
-- Records of config_info_tag
-- ----------------------------

-- ----------------------------
-- Table structure for config_tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `config_tags_relation`;
CREATE TABLE `config_tags_relation` (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `tag_name` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nid`),
  UNIQUE KEY `uk_configtagrelation_configidtag` (`id`,`tag_name`,`tag_type`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_tag_relation';

-- ----------------------------
-- Records of config_tags_relation
-- ----------------------------

-- ----------------------------
-- Table structure for group_capacity
-- ----------------------------
DROP TABLE IF EXISTS `group_capacity`;
CREATE TABLE `group_capacity` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配额，0表示使用默认值',
  `usage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用量',
  `max_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大变更历史数量',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='集群、各Group容量信息表';

-- ----------------------------
-- Records of group_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for his_config_info
-- ----------------------------
DROP TABLE IF EXISTS `his_config_info`;
CREATE TABLE `his_config_info` (
  `id` bigint(20) unsigned NOT NULL,
  `nid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `data_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `group_id` varchar(128) COLLATE utf8_bin NOT NULL,
  `app_name` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT 'app_name',
  `content` longtext COLLATE utf8_bin NOT NULL,
  `md5` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `src_user` text COLLATE utf8_bin,
  `src_ip` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `op_type` char(10) COLLATE utf8_bin DEFAULT NULL,
  `tenant_id` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT '租户字段',
  `encrypted_data_key` text COLLATE utf8_bin NOT NULL COMMENT '秘钥',
  PRIMARY KEY (`nid`),
  KEY `idx_gmt_create` (`gmt_create`),
  KEY `idx_gmt_modified` (`gmt_modified`),
  KEY `idx_did` (`data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='多租户改造';

-- ----------------------------
-- Records of his_config_info
-- ----------------------------
INSERT INTO `his_config_info` VALUES ('0', '1', 'system_info', 'DEFAULT_GROUP', '', 0x7666633A0D0A2020202073797374656D3A0D0A20202020202020206E616D653A20E9AB98E88083E5BF97E684BFE5A1ABE68AA50D0A202020202020202076657273696F6E3A20302E302E32, 'f84d0ddbdd19a37518b3879c94086fc9', '2023-10-24 11:50:32', '2023-10-24 03:50:33', null, '192.168.0.151', 'I', '', '');
INSERT INTO `his_config_info` VALUES ('1', '2', 'system_info', 'DEFAULT_GROUP', '', 0x7666633A0D0A2020202073797374656D3A0D0A20202020202020206E616D653A20E9AB98E88083E5BF97E684BFE5A1ABE68AA50D0A202020202020202076657273696F6E3A20302E302E32, 'f84d0ddbdd19a37518b3879c94086fc9', '2023-10-25 09:52:55', '2023-10-25 01:52:55', null, '192.168.1.199', 'D', '', '');
INSERT INTO `his_config_info` VALUES ('0', '3', 'consumer-score-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0D0A2020202073797374656D3A0D0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F0D0A202020202020202076657273696F6E3A2076302E302E32, '131fe764aed51633706473e86182b933', '2023-10-25 10:01:37', '2023-10-25 02:01:37', null, '192.168.1.199', 'I', '', '');
INSERT INTO `his_config_info` VALUES ('2', '4', 'consumer-score-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0D0A2020202073797374656D3A0D0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F0D0A202020202020202076657273696F6E3A2076302E302E32, '131fe764aed51633706473e86182b933', '2023-10-25 10:48:09', '2023-10-25 02:48:09', 0x6E61636F73, '192.168.1.199', 'U', '', '');
INSERT INTO `his_config_info` VALUES ('2', '5', 'consumer-score-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0A2020202073797374656D3A0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F310A202020202020202076657273696F6E3A2076302E302E32, '30c117bf45cc64f0a6d3462fbfc519b3', '2023-10-25 10:49:36', '2023-10-25 02:49:37', 0x6E61636F73, '192.168.1.199', 'U', '', '');
INSERT INTO `his_config_info` VALUES ('2', '6', 'consumer-score-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0A2020202073797374656D3A0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F320A202020202020202076657273696F6E3A2076302E302E32, '972e90eb667c9dc07ff0da3e4a8af20e', '2023-10-26 20:40:06', '2023-10-26 12:40:06', 0x6E61636F73, '192.168.0.151', 'U', '', '');
INSERT INTO `his_config_info` VALUES ('0', '7', 'system-info-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0D0A2020202073797374656D3A0D0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F320D0A202020202020202076657273696F6E3A2076302E302E32, '431529a918e37b94845167849cdc3f40', '2023-10-26 20:41:36', '2023-10-26 12:41:37', null, '192.168.0.151', 'I', '', '');
INSERT INTO `his_config_info` VALUES ('6', '8', 'system-info-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0D0A2020202073797374656D3A0D0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F320D0A202020202020202076657273696F6E3A2076302E302E32, '431529a918e37b94845167849cdc3f40', '2023-10-26 20:55:10', '2023-10-26 12:55:11', 0x6E61636F73, '192.168.0.151', 'U', '', '');
INSERT INTO `his_config_info` VALUES ('6', '9', 'system-info-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0A2020202073797374656D3A0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F320A202020202020202076657273696F6E3A2076302E302E33, '87b1e62a02557bdbf981edc03cc1eb61', '2023-10-26 20:57:46', '2023-10-26 12:57:46', 0x6E61636F73, '192.168.0.151', 'U', '', '');
INSERT INTO `his_config_info` VALUES ('6', '10', 'system-info-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0A2020202073797374656D3A0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F320A202020202020202076657273696F6E3A2076302E302E34, 'f8cb7ad4c1f5114ee96061d51f088b58', '2023-10-26 21:01:50', '2023-10-26 13:01:51', 0x6E61636F73, '192.168.0.151', 'U', '', '');
INSERT INTO `his_config_info` VALUES ('6', '11', 'system-info-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0A2020202073797374656D3A0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F320A202020202020202076657273696F6E3A2076302E302E35, '7d1f58003952518015a4a7817ba35fd3', '2023-10-26 21:12:25', '2023-10-26 13:12:25', 0x6E61636F73, '192.168.0.151', 'U', '', '');
INSERT INTO `his_config_info` VALUES ('6', '12', 'system-info-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0A2020202073797374656D3A0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F320A202020202020202076657273696F6E3A2076302E302E36, '07c71dc7dbec35970694a409dea2e795', '2023-10-26 21:17:12', '2023-10-26 13:17:12', 0x6E61636F73, '192.168.0.151', 'U', '', '');
INSERT INTO `his_config_info` VALUES ('6', '13', 'system-info-dev.yaml', 'DEFAULT_GROUP', '', 0x7666633A0A2020202073797374656D3A0A20202020202020206E616D653A20E5BF97E684BFE5A1ABE68AA5E7B3BBE7BB9F320A202020202020202076657273696F6E3A2076302E302E37, '8cd3ebc0c0134de750a54282333e3799', '2023-10-26 21:20:05', '2023-10-26 13:20:06', 0x6E61636F73, '192.168.0.151', 'U', '', '');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `role` varchar(50) NOT NULL,
  `resource` varchar(255) NOT NULL,
  `action` varchar(8) NOT NULL,
  UNIQUE KEY `uk_role_permission` (`role`,`resource`,`action`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of permissions
-- ----------------------------

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `username` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL,
  UNIQUE KEY `idx_user_role` (`username`,`role`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('nacos', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for tenant_capacity
-- ----------------------------
DROP TABLE IF EXISTS `tenant_capacity`;
CREATE TABLE `tenant_capacity` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配额，0表示使用默认值',
  `usage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用量',
  `max_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '聚合子配置最大个数',
  `max_aggr_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大变更历史数量',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_tenant_id` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='租户容量信息表';

-- ----------------------------
-- Records of tenant_capacity
-- ----------------------------

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) COLLATE utf8_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) COLLATE utf8_bin DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_tenant_info_kptenantid` (`kp`,`tenant_id`),
  KEY `idx_tenant_id` (`tenant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='tenant_info';

-- ----------------------------
-- Records of tenant_info
-- ----------------------------
INSERT INTO `tenant_info` VALUES ('1', '1', 'a423035a-c4a4-4dad-aae7-739a3764f75e', 'prod', '生产环境服务', 'nacos', '1698117890368', '1698117890368');
INSERT INTO `tenant_info` VALUES ('2', '1', '71d04c8c-1bce-4adc-9904-4573bf5a7885', 'test', '测试环境服务', 'nacos', '1698117901979', '1698117901979');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', '1');
