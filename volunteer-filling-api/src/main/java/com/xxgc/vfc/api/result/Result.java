package com.xxgc.vfc.api.result;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Sjy
 * @Date :2023/10/20 - 10 - 20 - 16:47
 * 统一返回结果
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
//如果值为空，在转json时就移除
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result<T> {

    private int code;

    private String msg;

    private Long count;

    private T data;

    private String errorMsg;
    //系统名称
    private String systemName;
    //系统版本
    private String systemVersion;


    //--------------改变部分信息-----------------
    public Result msg(String msg){
        this.msg = msg;
        return this;
    }
    public Result code(int code){
        this.code = code;
        return this;
    }
    public Result errorMsg(String errorMsg){
        this.errorMsg = errorMsg;
        return this;
    }
    public Result data(T data){
        this.data = data;
        return this;
    }
    public Result count(Long count){
        this.count = count;
        return this;
    }
    public Result systemName(String systemName){
        this.systemName = systemName;
        return this;
    }
    public Result systemVersion(String systemVersion){
        this.systemVersion = systemVersion;
        return this;
    }

    //---------------自定义构造函数------------------
    public Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    //请求成功的返回
    public static <T> Result<T> ok(T data){
        return new Result(200,"请求成功",data);
    }
    //请求成功的返回 不带参数
    public static <T> Result<T> ok(){
        return new Result(200,"请求成功",null);
    }

    //请求失败的返回 不带参数
    public static <T> Result<T> error(){
        return new Result(500,"请求失败",null);
    }
    //请求失败的返回 异常拦截使用
    public static <T> Result<T> error(int code,T data){
        return new Result(code,"请求失败",data);
    }
}
