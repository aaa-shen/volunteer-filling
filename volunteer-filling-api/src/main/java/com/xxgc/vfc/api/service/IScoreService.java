package com.xxgc.vfc.api.service;

/**
 * @Author: Sjy
 * @Date :2023/10/20 - 10 - 20 - 17:28
 * 获取分数相关业务逻辑
 */
public interface IScoreService {
    Integer getScoreByUserIdCard(String idCard);
}
